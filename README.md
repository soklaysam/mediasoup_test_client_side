## Description

Mediasoup testing (just for practice).
[demo_video](https://www.loom.com/share/35b309e558194e57aef5cda74264833d?sharedAppSource=personal_library)

[client_side code](https://bitbucket.org/soklaysam/mediasoup_test_client_side) using port 8080

[server_side code](https://bitbucket.org/soklaysam/mediasoup_test_server_side) using port 3000 and port 4000 (for socket-io)

## Installation

```bash
$ npm install
```

## Build Setup

```bash
# serve with hot reload at localhost:8080
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```