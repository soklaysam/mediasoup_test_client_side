export const state = () => ({
  username: ''
})

export const getters = {
  getUsername(state:any){
    return state.username;
  }
}

export const mutations = {
  setUsername(state:any, name:string){
    state.username = name;
  }
}